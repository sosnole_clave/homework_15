﻿#include <iostream>
using namespace std;
class Gamer
{
public:
    char nickname[100] = "word";
    int score = 0;
    void Enter()
    {
        cin >> nickname >> score;
    }
    void Display()
    {
        cout << nickname << "\t" << score;
    }
};

int main()
{
    int size = 0;
    cout << "Enter number of players ";
    cin >> size;
    Gamer* arr = new Gamer[size];
    for (int i = 0; i < size;i++)
    {
        cout << "Enter " << i + 1<< " " << "player nickname and score ";
        arr[i].Enter();
    }
    for (int i = 0; i < size; i++)
    {
        for (int j = 0; j < size - i - 1; j++)
        {
            if (arr[j].score > arr[j + 1].score)
            {
                swap(arr[j], arr[j + 1]);
            }
                
        }
    }
    for (int i = 0; i < size; i++)
    {
        arr[i].Display();
        cout << "\n";
    }
    delete[] arr;
    arr = nullptr;
}
